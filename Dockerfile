FROM python:3.7.3-slim-stretch

RUN apt-get update
#RUN apt-get install -y software-properties-common
#RUN apt install -y graphviz
#RUN apt install -y curl
#RUN apt install -y net-tools
RUN apt-get install -y gcc

#WORKDIR /opt/api-server-example
#WORKDIR /home/andrea/example_dev/test_andrea
WORKDIR .

COPY . ./

#RUN pip install -r requirements.txt
#RUN pip install -U memory_profiler
RUN pip install flask-restful 
RUN pip install requests 
RUN pip install jsonify
RUN pip install flask-reqparse
RUN pip install flask_restful

#RUN useradd data-layer
RUN chmod -R 777 /api_server_examplev2.py 

EXPOSE 5000

#USER data-layer

CMD ["python", "/api_server_examplev2.py"]
