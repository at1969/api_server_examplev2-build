from flask import Flask, request, jsonify, json
from flask_restful import reqparse
app = Flask(__name__)

users = [
    {
        "name": "Giorgio3",
        "age": 42,
        "occupation": "Network Engineer"
    },
    {
        "name": "Elena",
        "age": 32,
        "occupation": "Doctor"
    },
    {
        "name": "Fabio",
        "age": 22,
        "occupation": "Web Developer"
    }
]


@app.route('/create', methods=['GET','POST'])
def create():
  if request.method == 'POST':
    #check user details from db
    print (users)
    print ("Create POST")
    parser = reqparse.RequestParser()
    parser.add_argument("name")
    parser.add_argument("age")
    parser.add_argument("occupation")
    args = parser.parse_args()

    user = {
      "name": args["name"],
      "age": args["age"],
      "occupation": args["occupation"]
    }
    users.append(user)
    return user, 201


  elif request.method == 'GET':
    #serve login page
    print ("Create GET")
    return "Creaet eGET"

@app.route('/list', methods=['GET'])
def search():
    #serve login page
    print ("list GET")
    print ("type", type(users))
    for i in users: 
      print(i) 

    #json_object = json.loads(users)
    r=json.dumps(users) 
    print ("-----------")
    print (users)
    print ("-----------")

    return r, 200 

app.run(host='0.0.0.0', port=5000, debug=False)



